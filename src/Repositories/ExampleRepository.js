const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { ExampleSchema } = require('./../Models/example')


class ExampleRepository extends BaseRepository {
    constructor() {
        super('example', ExampleSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new ExampleRepository()