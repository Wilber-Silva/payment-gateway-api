const { Response, ResponseCreated, ResponseError } = require('will-core-lib/http')
const { BunkerCardLambda: BunkerCard } = require('will-internal-integrations')

module.exports.handler = async ({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        BunkerCard.key = platformKey
        const created = await BunkerCard.createCard(body)

        return new ResponseCreated(created)
    } catch (e) {
        return new ResponseError(e)
    }
}