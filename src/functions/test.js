const { ResponseOK } = require('will-core-lib/http')

module.exports.handler = async () => new ResponseOK({ test: 'ok' })
