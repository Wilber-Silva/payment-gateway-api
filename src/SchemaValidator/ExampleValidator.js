const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)

const login = Joi.string().max(256)
const custom = Joi.object()

module.exports.schema = Joi.object({
    login: login.required(),
    custom
})
