
const mongoose = require('mongoose')

module.exports.ExampleSchema = new mongoose.Schema({
    login: { type: String, require: true},

    // Required in all schemas 
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: null },
    deletedAt: { type: Date, default: null }
})
